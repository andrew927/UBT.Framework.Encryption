﻿using System;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace UBT.Encrypt.Tools
{
    public partial class FrmEncrypt : Form
    {


        public FrmEncrypt()
        {
            InitializeComponent();

            var longDate = DateTime.Now.ToLongDateString();
            var longTime = DateTime.Now.ToLongTimeString();
            var formatDate = DateTime.Now.ToString("yyyyMMddHHmmssffff");
        }

        /// <summary>
        /// 生成密钥
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            string unEncryStr = this.txtUnEncryStr.Text.Trim();

            if (unEncryStr == "")
            {
                MessageBox.Show("请输入待加密的字符串!");
                return;
            }

            //深度加密
            var secondEncryStr = DESEncrypt.DepthEncrypt(unEncryStr);

            this.txtEncryStr.Text = secondEncryStr;



        }

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDecrypt_Click(object sender, EventArgs e)
        {
            var str = txtEncryStr.Text;
            var yetDecrytString = DESEncrypt.DepthDecrypt(str);
            txtDecryptStr.Text = yetDecrytString;
        }




    }
}
